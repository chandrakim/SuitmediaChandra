package com.chandralim.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.chandralim.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends ActionBarActivity {

    private static int EVENT_ACTIVITY = 1;
    private static int GUEST_ACTIVITY = 2;

    @Bind(R.id.txtUsername) protected TextView txtUsername;
    @Bind(R.id.btnEvent) protected Button btnEvent;
    @Bind(R.id.btnGuest) protected Button btnGuest;
    @Bind(R.id.tool_bar) protected View view;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("Main");
        toolbar.setTitleTextColor(Color.rgb(197, 180, 87));
        setSupportActionBar(toolbar);

        Intent in = getIntent();
        Log.e("string",in.getStringExtra("username"));

        txtUsername.setText(in.getStringExtra("username"));

        btnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),EventActivity.class);

                startActivityForResult(intent, EVENT_ACTIVITY);
            }
        });

        btnGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),GuestActivity.class);

                startActivityForResult(intent, GUEST_ACTIVITY);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (1) : {
                if (resultCode == Activity.RESULT_OK) {
                    String eventName = data.getStringExtra("eventName");

                    btnEvent.setText(eventName);
                }
                break;
            }
            case (2) : {
                if (resultCode == Activity.RESULT_OK) {
                    String guestName = data.getStringExtra("guestName");
                    String birthdate = data.getStringExtra("birthdate");

                    String[] bd = new String[3];
                    bd = birthdate.split("-");


                    int tanggal = Integer.parseInt(bd[2]);
                    int bulan = Integer.parseInt(bd[1]);
                    String hasil;
                    if(tanggal % 2 == 0 && tanggal % 3 == 0){
                        hasil = "iOS";
                    }else if(tanggal % 2 ==0){
                        hasil = "Blackberry";
                    }else if(tanggal % 3 ==0){
                        hasil = "Android";
                    }else{
                        hasil = "Feature Phone";
                    }



                    boolean prime = isPrime(bulan);

                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Result");
                    alertDialog.setMessage("Tanggal "+bd[2] + " " + hasil + "\n\n"  + bulan+ " Bulan prime " + prime);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

//                                    Intent intent = new Intent(getBaseContext(),MainActivity.class);
//                                    intent.putExtra("username", username);
//
//                                    startActivity(intent);
                                }
                            });
                    alertDialog.show();
                    btnGuest.setText(guestName);
                }
                break;
            }
        }
    }
    public boolean isPrime(int bulan) {
        int sqrt = (int) Math.sqrt(bulan) + 1;

        if(bulan == 1){
            return false;
        }else{
            for (int i = 2; i <  sqrt; i++){
                if (bulan % i == 0) {


                    return false;
                }
            }
            return true;
        }



    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
