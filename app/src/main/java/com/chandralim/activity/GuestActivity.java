package com.chandralim.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.chandralim.R;
import com.chandralim.adapter.EventAdapter;
import com.chandralim.adapter.GuestAdapter;
import com.chandralim.interfaces.ApiService;
import com.chandralim.model.Event;
import com.chandralim.model.Guest;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GuestActivity extends ActionBarActivity {

    @Bind(R.id.tool_bar) protected View view;
    @Bind(R.id.gridView) protected GridView gridView;
    private Toolbar toolbar;
    private ProgressDialog dialog;
    private Activity self;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);
        ButterKnife.bind(this);
        self = this;

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("Guest");
        toolbar.setTitleTextColor(Color.rgb(197, 180, 87));
        toolbar.setNavigationIcon(R.drawable.button_back_selector);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getData();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Guest guest = (Guest) adapterView.getItemAtPosition(position);

                Intent resultIntent = new Intent();
                resultIntent.putExtra("guestName", guest.getName());
                resultIntent.putExtra("birthdate", guest.getBirthdate());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getData() {

        dialog = new ProgressDialog(GuestActivity.this, AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(true);
        dialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dry-sierra-6832.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<List<Guest>> listGuests = service.listGuests();
        listGuests.enqueue(new Callback<List<Guest>>() {
            @Override
            public void onResponse(Call<List<Guest>> call, Response<List<Guest>> response) {

                if (response.isSuccessful()) {
                    List<Guest> listGuest = response.body();

                    int[] images = {R.drawable.foto_1, R.drawable.foto_2, R.drawable.foto_3, R.drawable.foto_4, R.drawable.foto_5};

                    for (int i = 0; i < listGuest.size(); i++) {
                        Guest guest = listGuest.get(i);

                        guest.setImage(images[i]);
                    }

                    GuestAdapter adapter = new GuestAdapter(self, listGuest);
                    gridView.setAdapter(adapter);
                }

                if (dialog.isShowing())
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Guest>> call, Throwable t) {
                dialog.hide();
            }
        });
    }
}
