package com.chandralim.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.chandralim.R;
import com.chandralim.adapter.EventAdapter;
import com.chandralim.interfaces.ApiService;
import com.chandralim.model.Event;
import com.chandralim.model.Guest;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EventActivity extends ActionBarActivity {

    @Bind(R.id.tool_bar) protected View view;
    @Bind(R.id.listView) protected ListView listView;
    private Toolbar toolbar;
    private ProgressDialog dialog;
    private Activity self;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        ButterKnife.bind(this);
        self = this;

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("Event");
        toolbar.setTitleTextColor(Color.rgb(197, 180, 87));
        toolbar.setNavigationIcon(R.drawable.button_back_selector);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Event event = (Event) adapterView.getItemAtPosition(position);

                Intent resultIntent = new Intent();
                resultIntent.putExtra("eventName", event.getName());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getData() {

        dialog = new ProgressDialog(EventActivity.this, AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Loading");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(true);
        dialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://suitmedia-test.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<List<Event>> listEvents = service.listEvents();
        listEvents.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {

                if (response.isSuccessful()) {
                    List<Event> listEvent = response.body();

                    EventAdapter adapter = new EventAdapter(self, listEvent);
                    listView.setAdapter(adapter);
                }

                if (dialog.isShowing())
                    dialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                //dialog.hide();
            }
        });
    }
}
