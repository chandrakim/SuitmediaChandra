package com.chandralim.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chandralim.R;
import com.chandralim.model.Guest;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Chandra Lim on 26-Mar-16.
 */
public class GuestAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<Guest> guestList;

    public GuestAdapter(Activity activity, List<Guest> guestList) {
        this.activity = activity;
        this.guestList = guestList;
    }

    @Override
    public int getCount() {
        return guestList.size();
    }

    @Override
    public Object getItem(int location) {
        return guestList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_guests, null);

        TextView text = (TextView) convertView.findViewById(R.id.text);
        ImageView image = (ImageView) convertView.findViewById(R.id.image);

        Guest guest = guestList.get(position);

        text.setText(guest.getName());

        image.setImageResource(guest.getImage());

        return convertView;
    }
}
