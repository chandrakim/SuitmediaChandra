package com.chandralim.interfaces;

import com.chandralim.model.Event;
import com.chandralim.model.Guest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Chandra Lim on 26-Mar-16.
 */
public interface ApiService {

    @GET("api/people")
    Call<List<Guest>> listGuests();

    @GET("/events")
    Call<List<Event>> listEvents();
}
